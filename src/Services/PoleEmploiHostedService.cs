using Microsoft.Extensions.Options;
using tth.Models.Settings;
using tth.Services.Interfaces;

namespace tth.Services;

public class PoleEmploiHostedService : IHostedService, IDisposable
{
    private readonly ILogger<PoleEmploiHostedService> _logger;
    private readonly PoleEmploiHostedServiceSettings _settings;
    private readonly IPoleEmploiService _service;

    private Timer _timer = null!;

    public PoleEmploiHostedService(
        ILogger<PoleEmploiHostedService> logger,
        IOptions<PoleEmploiHostedServiceSettings> settings,
        IPoleEmploiService service)
    {
        _logger = logger;
        _settings = settings.Value;
        _service = service;
    }

    public Task StartAsync(CancellationToken stoppingToken)
    {
        _timer = new Timer(DoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(_settings.Period));

        return Task.CompletedTask;
    }

    private void DoWork(object? state)
    {
        Task.Run(() => _service.UpdateJobOffers()).GetAwaiter().GetResult();
    }

    public Task StopAsync(CancellationToken stoppingToken)
    {
        _timer?.Change(Timeout.Infinite, 0);

        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose();
    }
}
