using System.Net;
using System.Net.Http.Headers;
using System.Text.Json;
using Microsoft.Extensions.Options;
using tth.Data.Interfaces;
using tth.Models;
using tth.Models.Dtos;
using tth.Models.Settings;
using tth.Services.Interfaces;

namespace tth.Services;

public class PoleEmploiService : IPoleEmploiService
{
    private readonly ILogger<PoleEmploiService> _logger;
    private readonly PoleEmploiSettings _settings;
    private readonly IJobOffersRepository _repo;
    private readonly HttpClient _httpClient;

    private readonly IDictionary<string, string> _refreshTokenParams;

    private const int RETRIES = 3;

    public PoleEmploiService(
        ILogger<PoleEmploiService> logger,
        IOptions<PoleEmploiSettings> settings,
        IJobOffersRepository repo,
        IHttpClientFactory httpClientFactory)
    {
        _logger = logger;
        _settings = settings.Value;
        _repo = repo;
        _httpClient = httpClientFactory.CreateClient();

        _refreshTokenParams = new Dictionary<string, string>
        {
            { "grant_type", "client_credentials" },
            { "client_id", _settings.ClientId },
            { "client_secret", _settings.ClientSecret },
            { "scope", string.Join(' ', _settings.Scopes) }
        };

        Task.Run(() => RefreshAuthentication()).GetAwaiter().GetResult();
    }

    public async Task UpdateJobOffers()
    {
        var jobOffers = new List<JobOffer>();

        foreach (var locality in _settings.Localities)
        {
            var localityJobOffers = await GetJobOffers(locality);

            jobOffers.AddRange(localityJobOffers);

            _logger.LogInformation($"Got {localityJobOffers.Count} job offers for {locality} locality");
        }

        _logger.LogInformation($"Got a total of {jobOffers.Count} job offers");

        if (jobOffers == null)
            return;

        foreach (var jobOffer in jobOffers)
        {
            var alreadyExists = await _repo.ExistsAsync(jobOffer.Id);

            if (!alreadyExists)
            {
                _logger.LogInformation($"Adding new job offer {jobOffer.Id}");
                await _repo.CreateAsync(jobOffer);
            }
        }
    }

    private async Task<List<JobOffer>?> GetJobOffers(string locality)
    {
        var uri = string.Format(
            $"{_settings.JobOffersSearchUrl}?commune={locality}&distance=0");

        var response = await PoleEmploiCallAsync(() => _httpClient.GetAsync(Uri.EscapeUriString(uri)));
        var contentResponse = await response.Content.ReadAsStringAsync();

        if (!response.IsSuccessStatusCode)
            throw new HttpRequestException(contentResponse);

        var jobOffersResponse = JsonSerializer.Deserialize<JobOffersResponse>(contentResponse);

        if (jobOffersResponse == null)
            throw new HttpRequestException(contentResponse);

        return jobOffersResponse.Results;
    }

    private async Task<HttpResponseMessage> PoleEmploiCallAsync(Func<Task<HttpResponseMessage>> poleEmploiCall)
    {
        for (var tryCount = 0; tryCount < RETRIES; tryCount++)
        {
            var response = await poleEmploiCall();

            if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
            {
                await RefreshAuthentication();
                continue;
            }

            return response;
        }

        _logger.LogError($"Failed to refresh pole emploi authentication");

        throw new UnauthorizedAccessException();
    }

    private async Task RefreshAuthentication()
    {
        _logger.LogInformation($"Refreshing pole emploi authentication");

        var requestUri = new Uri(_settings.AccessTokenUrl);
        var content = new FormUrlEncodedContent(_refreshTokenParams);

        var response = await _httpClient.PostAsync(requestUri, content);
        var contentResponse = await response.Content.ReadAsStringAsync();

        if (!response.IsSuccessStatusCode)
            throw new HttpRequestException(contentResponse);

        var accessTokenResponse = JsonSerializer.Deserialize<PoleEmploiAccessTokenResponse>(contentResponse);

        if (accessTokenResponse == null)
            throw new HttpRequestException(contentResponse);

        _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessTokenResponse.AccessToken);

        _logger.LogInformation($"Successfully refreshed pole emploi authentication");
    }
}
