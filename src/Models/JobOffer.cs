using System.Text.Json.Serialization;

namespace tth.Models;

public class JobOffer
{
    [JsonPropertyName("id")]
    public string? Id { get; set; }

    [JsonPropertyName("description")]
    public string? Description { get; set; }

    [JsonPropertyName("typeContrat")]
    public string? ContractType { get; set; }

    [JsonPropertyName("typeContratLibelle")]
    public string? ContractTypeText { get; set; }

    [JsonPropertyName("contact")]
    public Contact? Contact { get; set; }

    [JsonPropertyName("entreprise")]
    public Company? Company { get; set; }
}

public class Company
{
    [JsonPropertyName("nom")]
    public string? Name { get; set; }

    [JsonPropertyName("description")]
    public string? Description { get; set; }

    [JsonPropertyName("entrepriseAdaptee")]
    public string? AdaptedCompany { get; set; }
}

public class Contact
{
    [JsonPropertyName("coordonnees1")]
    public string? Coordonnees { get; set; }

    [JsonPropertyName("urlPostulation")]
    public string? UrlPostulation { get; set; }
}
