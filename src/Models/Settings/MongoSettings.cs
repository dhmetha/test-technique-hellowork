namespace tth.Models.Settings;

public class MongoSettings
{
    public string ServerAddress { get; set; } = null!;
    public int ServerPort { get; set; }
    public string Username { get; set; } = null!;
    public string Password { get; set; } = null!;
    public string AuthenticationDatabaseName { get; set; } = null!;
    public string DatabaseName { get; set; } = null!;

    public string JobOffersCollection { get; set; } = null!;
}
