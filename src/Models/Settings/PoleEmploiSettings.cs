namespace tth.Models.Settings;

public class PoleEmploiSettings
{
    public string ClientId { get; set; } = null!;
    public string ClientSecret { get; set; } = null!;
    public List<string> Scopes { get; set; } = null!;
    public List<string> Localities { get; set; } = null!;
    public string AccessTokenUrl { get; set; } = null!;
    public string JobOffersSearchUrl { get; set; } = null!;
}
