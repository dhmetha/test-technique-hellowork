using System.Text.Json.Serialization;

namespace tth.Models.Dtos;

public class JobOffersResponse
{
    [JsonPropertyName("resultats")]
    public List<JobOffer>? Results { get; set; }

    [JsonPropertyName("filtresPossibles")]
    public string? Commune { get; set; }
}
