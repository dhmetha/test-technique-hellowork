﻿using tth.Data;
using tth.Data.Interfaces;
using tth.Models.Settings;
using tth.Services;
using tth.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

var configuration = builder.Configuration;
var services = builder.Services;

services.Configure<MongoSettings>(configuration.GetSection(nameof(MongoSettings)));
services.Configure<PoleEmploiSettings>(configuration.GetSection(nameof(PoleEmploiSettings)));
services.Configure<PoleEmploiHostedServiceSettings>(configuration.GetSection(nameof(PoleEmploiHostedServiceSettings)));

services.AddHttpClient();

services.AddSingleton<IJobOffersRepository, JobOffersRepository>();
services.AddSingleton<IPoleEmploiService, PoleEmploiService>();

services.AddHostedService<PoleEmploiHostedService>();

var app = builder.Build();

app.Run();
