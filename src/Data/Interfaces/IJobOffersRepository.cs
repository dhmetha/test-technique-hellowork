using tth.Models;

namespace tth.Data.Interfaces;

public interface IJobOffersRepository
{
    public Task<List<JobOffer>> GetAsync();
    public Task<JobOffer?> GetAsync(string id);
    public Task<bool> ExistsAsync(string id);
    public Task CreateAsync(JobOffer newJobOffer);
    public Task UpdateAsync(string id, JobOffer updatedJobOffer);
    public Task RemoveAsync(string id);
}
