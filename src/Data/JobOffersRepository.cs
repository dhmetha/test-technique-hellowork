using System.Text.Json;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using tth.Data.Interfaces;
using tth.Models;
using tth.Models.Settings;

namespace tth.Data;

public class JobOffersRepository : IJobOffersRepository
{
    private readonly IMongoCollection<JobOffer> _collection;

    public JobOffersRepository(
        IOptions<MongoSettings> mongoSettings)
    {
        var s = mongoSettings.Value;

        Console.WriteLine(JsonSerializer.Serialize(s));

        var mongoConnectionString = $"mongodb://{s.Username}:{s.Password}@{s.ServerAddress}:{s.ServerPort}/{s.AuthenticationDatabaseName}";
        var mongoUrl = new MongoUrl(mongoConnectionString);
        var mongoClient = new MongoClient(mongoUrl);

        var database = mongoClient.GetDatabase(s.DatabaseName);
        _collection = database.GetCollection<JobOffer>(s.JobOffersCollection);
    }

    public async Task<List<JobOffer>> GetAsync()
        => await _collection.Find(_ => true).ToListAsync();

    public async Task<JobOffer?> GetAsync(string id)
        => await _collection.Find(x => x.Id == id).FirstOrDefaultAsync();

    public async Task<bool> ExistsAsync(string id)
        => await GetAsync(id) != null;

    public async Task CreateAsync(JobOffer newJobOffer)
        => await _collection.InsertOneAsync(newJobOffer);

    public async Task UpdateAsync(string id, JobOffer updatedJobOffer)
        => await _collection.ReplaceOneAsync(x => x.Id == id, updatedJobOffer);

    public async Task RemoveAsync(string id)
        => await _collection.DeleteOneAsync(x => x.Id == id);
}
